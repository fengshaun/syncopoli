An rsync client that allows you to backup files/directories on your phone to your remote server.

Features:

* Phone to remote server backup
* Remote server to phone replication
* Supports transport through both Rsync and SSH protocols
* Supports authentication by password or public key
